﻿using ConsoleAppWithDb.Repositories;
using FirstMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.Repositories
{
    public class BrandRepository : Repository<Brand>, IBrandRepository
    {
        public BrandRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Brands;
        }

    }
}
