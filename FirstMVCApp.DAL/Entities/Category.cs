﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace FirstMVCApp.DAL.Entities
{
    public class Category : Entity
    {
        public string Name { get; set; }
        public ICollection<Product> products { get; set; }
    }
}
