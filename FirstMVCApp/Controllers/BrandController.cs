﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class BrandController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var Brands = uow.Brands.GetAll().ToList();
                return View(Brands);
            }
        }
    }
}