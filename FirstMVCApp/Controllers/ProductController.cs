﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Configuration;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;
using FirstMVCApp.Models.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{

    public class ProductController : Controller
    {
        private readonly IProductService _productService;


        public ProductController(IProductService productService)
        {
            if (productService == null)
                throw new ArgumentNullException(nameof(productService));
            
            _productService = productService; 
            
        }

        [Route("Search/{name?}/{CategoryId?}/{BarndId?}/{priceFrom?}/{priceTo?}")]
        [Route("Product/Index")]
        public IActionResult Index([FromRoute]ProductFilterModel model)
        {
            try
            {
                var models = _productService.SearchProduct(model);
                return View(models);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}