﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CategoryController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var categories = uow.Categories.GetAll().ToList();
                return View(categories);
            }
        }
    }
}