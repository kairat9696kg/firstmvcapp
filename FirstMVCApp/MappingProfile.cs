﻿using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            CreateProductToProductModelMap();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(pm => pm.BrandName,
                src => src.MapFrom(p => p.BrandId != null ? p.Brand.Name : ProductModel.NoBrand));
            CreateMap<Product, ProductModel>()
               .ForMember(pm => pm.CategoryName,
               src => src.MapFrom(p => p.Category.Name));
        }
    }
}
