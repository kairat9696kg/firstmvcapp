﻿using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models.Services
{
    public interface IProductService
    {
        public List<ProductModel> SearchProduct(ProductFilterModel model);
    }
    public class ProductService : IProductService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ProductService(IUnitOfWorkFactory unitOfWorkFactory)
        {

            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<ProductModel> SearchProduct(ProductFilterModel model)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {

                IEnumerable<Product> products = uow.Products.GetAllWithCategoryAndBrands();
                products = products.ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByName(model.Name)
                    .ByCategoryId(model.CategoryId, uow)
                    .ByBrandId(model.BrandId, uow);


                List<ProductModel> productModels = Mapper.Map<List<ProductModel>>(products);
                return productModels;
            }
        }
    }
}
